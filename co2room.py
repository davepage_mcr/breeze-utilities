#!/usr/bin/env python3

import argparse

###################################################
# A Room contains three gases, measured in litres #
# - Fresh / Filtered CO2 (fc)                     #
# - Exhaled CO2 (xc)                              #
# - Non-CO2 (o)                                   #
###################################################

class Room:

    def __init__(self, volume):
        self.volume = volume

        # Fresh air contains 0.04% CO2 by volume === 400 ppm
        self.fc = volume * 400 / 1000000
        self.xc = 0
        self.o = volume - self.fc

        self.check()

    def check(self):
        assert( self.fc >= 0 )
        assert( self.xc >= 0 )
        assert( self.o  >= 0 )
        assert( self.fc + self.xc + self.o < self.volume + 1 )
        assert( self.fc + self.xc + self.o > self.volume - 1 )

    def ppm(self):
        # Parts per million CO2 that would be measured in this room
        return int( ( self.fc + self.xc ) * 1000000 / self.volume )

    def xcppm(self):
        # Parts per million of exhaled CO2 - not directly measurable
        # but the thing we're trying to actually avoid!
        return int( self.xc * 1000000 / self.volume )

    def respire(self, respvolume):
        # People remove respvolume of the room's gases, and replace Non-CO2 with 4% Exhaled CO2
        replaced = respvolume * ( self.o / self.volume ) * 0.04
        self.xc += replaced
        self.o -= replaced

        self.check()

    def ventilate(self, ventvolume):
        # Extraction removes ventvolume of the room's gases
        # replaces with equivalent volume, 99.96% non-CO2, 0.04% fc, 0% xc

        xcremoved = ventvolume * ( self.xc / self.volume )
        fcremoved = ventvolume * ( self.fc / self.volume )
        fcreplaced = ventvolume * 0.0004
        oremoved = ventvolume * ( self.o / self.volume )
        oreplaced = ventvolume * 0.9996

        self.xc -= xcremoved
        self.o = self.o - oremoved + oreplaced
        self.fc = self.fc - fcremoved + fcreplaced

        self.check()

    def filter(self, filtvolume, filtrate):
        # Process filtvolume L of the room's gases
        # Replace filtrate% of exhaled CO2 with filtered CO2

        xcfiltered = filtvolume * ( self.xc / self.volume ) * ( filtrate / 100 )

        self.xc -= xcfiltered
        self.fc += xcfiltered

        self.check()

##################################
# Main Program Logic Starts Here #
##################################

parser = argparse.ArgumentParser()
parser.add_argument("--csv",        help="CSV output", action='store_true', default=False)
parser.add_argument("--volume",     help="Volume of room (litres)", type=int, default=1000000)
parser.add_argument("--people",     help="Number of humans breathing in the room", type=int, default=50)
parser.add_argument("--minvol",     help="Minute Volume of each human (litres of air respired in one minute", type=int, default=10)
parser.add_argument("--ventilate",  help="Air ventilation rate (litres/minute)", type=int, default=0)
parser.add_argument("--maxppm",     help="Maximum PPM before we give up", type=int, default=2500)
parser.add_argument("--length",     help="Length of event in minutes", type=int, default=480)
parser.add_argument("--filter",     help="Filtration volume (L/min)", type=int, default=0)
parser.add_argument("--filtrate",   help="Filtration rate (%age)", type=float, default=99)
args = parser.parse_args()

room = Room(args.volume)
minute = 0

outstring="Minute {}, room CO2 is {} ppm, exhaled CO2 is {} ppm"
if args.csv:
    print("Minute,Room CO2,Exhaled CO2")
    outstring = "{},{},{}"

while room.ppm() < args.maxppm and minute <= args.length:
    print(outstring.format( minute, room.ppm(), room.xcppm()) )

    room.respire( args.people * args.minvol )
    room.ventilate( args.ventilate )
    room.filter( args.filter, args.filtrate )

    minute += 1

if not args.csv:
	if minute > args.length:
	    print("You kept the air under {} for {} minutes, well done".format(args.maxppm, args.length))
	else:
	    print("Your air quality went over {} PPM! Fail!".format(args.maxppm))

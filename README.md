Breeze Utilities
================

This is a repo containing scripts which might be useful when trying to figure
out stuff around ventilation and CO2 monitoring.

co2room.py
----------

You have a room. It has people in it. They are breathing. There is a certain
level of ventilation, either natural or extracted. How quickly will CO2
readings increase, and what changes if you improve the ventilation?

This is a very simple simulation of CO2 levels in a room. It's useful to see if
the CO2 level will rise over a threshold within a time period.
